const s = function(sketch) {
    sketch.setup = function() {
        let canvas = sketch.createCanvas(600,300)
        canvas.parent("circles")
        sketch.noLoop()
    };

    sketch.draw = function() {
        sketch.fill("steelblue")
        var data = [{id:"a", x:44, y:150},{id:"b",x:151,y:119},{id:"c",x:85,y:21}]
        data.forEach(function(d) {
        sketch.ellipse(d.x, d.y, 30, 30)
    })

    };
};
let myp5 = new p5(s);

// function setup() {
//     createCanvas(600,300)
//     noLoop()
// };

// function draw() {
//     fill("steelblue")
//     var data = [{id:"a", x:44, y:150},{id:"b",x:151,y:119},{id:"c",x:85,y:21}]
//     forEach(function(d) {
//         ellipse(d.x, d.y, 30, 30)
//     })
// };
