# Teaching material for HTML, CSS, javascript and SVG
This material is used to give an introduction to HTML, CSS, javascript and SVG. All HTML source code is contained in the file `htmlsources.html`, from which sections can be copied to `index.html`.

## Usage instructions
- Install Visual Studio Code from [code.visualstudio.com](http://code.visualstudio.com).
- Install the Live Server in VS Code (see [marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer))
- Follow instructions of Live Server page to start the server.
- Copy a section delimited by `<html>` and `</html>` from `htmlsources.html` to `index.html`, save, and check the webpage that opened when you started the server.
- Also open the Developer Tools in your browser (in Chrome: View -> Developer -> Developer Tools) to get access to:
  - the *console*. Error messages will appear here, and you can print things there yourself using `console.log("some text")` in your javascript code.
  - the *elements*. This is the underlying source code of the page, which will be the same as what is in the `index.html` file. When you hover over an element in this source that element will be highlighted in your webpage.